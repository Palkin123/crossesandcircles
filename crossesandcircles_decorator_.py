import sys
a = [1, 2, 3, 4, 5, 6, 7, 8, 9]
b = ['-', '-', '-', '-', '-', '-', '-', '-', '-']
print("Чтобы начать игру, напишите start(game)")
def start(func):
    print("Игра началась")
    func()
def make_move_cross():
    try:
        x = int(input"Ход крестиков")
    except ValueError:
        print("Это не число")
        make_move_cross()
    if (a[x-1] == 11) or (a[x-1] == 12):
        print ("Поле занято")
        make_move_cross()
    else:
        a[x-1] = 11
        b[x-1] = 'X'
    if a[0] == a[1] and a[1] == a[2] or \
       a[0] == a[3] and a[3] == a[6] or \
       a[0] == a[4] and a[4] == a[8] or \
       a[6] == a[7] and a[7] == a[8] or \
       a[6] == a[4] and a[4] == a[2] or \
       a[3] == a[4] and a[4] == a[5] or \
       a[1] == a[4] and a[4] == a[7] or \
       a[2] == a[5] and a[5] == a[8]:
        print ('\n', b[6], b[7], b[8], '\n', b[3],
               b[4], b[5], '\n', b[0], b[1], b[2])
        print ("Крестики победили")
        sys.exit()
        return (a)
    return (a)


def make_move_circle():
    y = int(input("Ход ноликов "))
    if (a[y-1] == 11) or (a[y-1] == 12):
        print ("Поле занято")
        make_move_circle()
    else:
        a[y-1] = 12
        b[y-1] = 'O'
    if a[0] == a[1] and a[1] == a[2] or \
       a[0] == a[3] and a[3] == a[6] or \
       a[0] == a[4] and a[4] == a[8] or \
       a[6] == a[7] and a[7] == a[8] or \
       a[6] == a[4] and a[4] == a[2] or \
       a[3] == a[4] and a[4] == a[5] or \
       a[1] == a[4] and a[4] == a[7] or \
       a[2] == a[5] and a[5] == a[8]:
        print ('\n', b[6], b[7], b[8], '\n',
               b[3], b[4], b[5], '\n', b[0], b[1], b[2])
        print ("Нолики победили")
        sys.exit()
        return (a)
    return (a)
def game():
    for j in range(5):
        make_move_cross()
        print('\n', b[6], b[7], b[8], '\n',
              b[3], b[4], b[5], '\n', b[0], b[1], b[2])
        if j == 4:
            break
        make_move_circle()
        print('\n', b[6], b[7], b[8], '\n',
              b[3], b[4], b[5], '\n', b[0], b[1], b[2])
    print("Ничья")
