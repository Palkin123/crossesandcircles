import sys
print("Чтобы начать игру, напишите main()")


class RangeError(Exception):
    def __init__(self, text):
        self.text = text


class GameComponents:
    def __init__(self, pos, graph):
        self.pos = pos
        self.graph = graph

    def make_move_cross(self):
        try:
            x = int(input("Ход крестиков "))
            if x > 9 or x < 1:
                raise RangeError("Введите число от 1 до 9")
        except ValueError:
            print("Это не число")
            GameComponents.make_move_cross(self)
        except RangeError as mr:
            print(mr)
            GameComponents.make_move_cross(self)
        else:
            if (self.pos[x - 1] == 11) or (self.pos[x - 1] == 12):
                print("Поле занято")
                GameComponents.make_move_cross(self)
            else:
                self.pos[x - 1] = 11
                self.graph[x - 1] = 'X'
            if self.pos[0] == self.pos[1] and self.pos[1] == self.pos[2] or \
                    self.pos[0] == self.pos[3] and self.pos[3] == self.pos[6] or \
                    self.pos[0] == self.pos[4] and self.pos[4] == self.pos[8] or \
                    self.pos[6] == self.pos[7] and self.pos[7] == self.pos[8] or \
                    self.pos[6] == self.pos[4] and self.pos[4] == self.pos[2] or \
                    self.pos[3] == self.pos[4] and self.pos[4] == self.pos[5] or \
                    self.pos[1] == self.pos[4] and self.pos[4] == self.pos[7] or \
                    self.pos[2] == self.pos[5] and self.pos[5] == self.pos[8]:
                GameComponents.game_image(self)
                print("Крестики победили")
                sys.exit()
        return self.pos

    def make_move_circle(self):
        try:
            y = int(input("Ход ноликов "))
            if y > 9 or y < 1:
                raise RangeError("Введите число от 1 до 9")
        except ValueError:
            print("Это не число")
            GameComponents.make_move_circle(self)
        except RangeError as mr:
            print(mr)
            GameComponents.make_move_circle(self)
        else:
            if (self.pos[y - 1] == 11) or (self.pos[y - 1] == 12):
                print("Поле занято")
                GameComponents.make_move_circle(self)
            else:
                self.pos[y - 1] = 12
                self.graph[y - 1] = 'O'
            if self.pos[0] == self.pos[1] and self.pos[1] == self.pos[2] or \
                    self.pos[0] == self.pos[3] and self.pos[3] == self.pos[6] or \
                    self.pos[0] == self.pos[4] and self.pos[4] == self.pos[8] or \
                    self.pos[6] == self.pos[7] and self.pos[7] == self.pos[8] or \
                    self.pos[6] == self.pos[4] and self.pos[4] == self.pos[2] or \
                    self.pos[3] == self.pos[4] and self.pos[4] == self.pos[5] or \
                    self.pos[1] == self.pos[4] and self.pos[4] == self.pos[7] or \
                    self.pos[2] == self.pos[5] and self.pos[5] == self.pos[8]:
                game_image(self)
                print("Нолики победили")
                sys.exit()
        return self.pos

    def game_image(self):
        print('\n', self.graph[6], self.graph[7], self.graph[8], '\n',
              self.graph[3], self.graph[4], self.graph[5], '\n', self.graph[0], self.graph[1], self.graph[2])


def main():
    game = GameComponents([1, 2, 3, 4, 5, 6, 7, 8, 9], ['-', '-', '-', '-', '-', '-', '-', '-', '-'])
    for j in range(5):
        game.make_move_cross()
        game.game_image()
        if j == 4:
            break
        game.make_move_circle()
        game.game_image()
    print("Ничья")
